import numpy as np


def stiffness(coords, blocks, dloads=None):
    """Assemble the global finite element stiffness

    Parameters
    ----------
    coords : ndarray
        Nodal coordinates.  coords[n, i] is the ith coordinate of node n
    blocks : list of element.block
        blocks[i] is the ith element block
    dloads : fetk.dload.dloads
        Distributed loads.

    Returns
    -------
    stiff : ndarray
        Global stiffness matrix stored as a full (nd*n, nd*n) symmetric matrix,
        where nd is the number of degrees of freedom per node and n the total
        number of nodes.

    """
    num_dofs = len(coords) * blocks[0].element.num_dof_per_node
    K = np.zeros((num_dofs, num_dofs), dtype=float)
    for block in blocks:
        Kb = block.stiffness(coords, dloads)
        dofs = block.global_dof_map()
        K[np.ix_(dofs, dofs)] += Kb
    return K


def force(coords, blocks, dloads):
    """Assemble global finite element source

    Parameters
    ----------
    coords : ndarray
        Nodal coordinates.  coords[n, i] is the ith coordinate of node n
    blocks : list of element.block
        blocks[i] is the ith element block
    dloads : fetk.dload.dloads
        Distributed loads.

    Returns
    -------
    F : ndarray
        Global force arrays tored as a full (nd x n) array, where nd is the number of
        degrees of freedom per node and n the total number of nodes.

    """
    num_nodes = coords.shape[0]
    num_dof_per_node = blocks[0].element.num_dof_per_node
    num_dofs = num_nodes * num_dof_per_node
    F = np.zeros(num_dofs, dtype=float)
    for block in blocks:
        num_dof_per_node = block.element.num_dof_per_node
        num_nodes = block.nodes.shape[0]
        num_dofs = num_nodes * num_dof_per_node
        fb = np.zeros(num_dofs)
        for (iel, xel) in enumerate(block.elements):
            dload = dloads.get(xel)
            if dload is not None:
                nodes = block.connect[iel]
                xc = coords[nodes]
                fe = block.element.force(xc, dload)
                fb[block.ix1d(iel)] += fe
        dofs = block.global_dof_map()
        F[np.ix_(dofs)] += fb

    return F
