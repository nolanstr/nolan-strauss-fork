import numpy as np

import fetk.dload
from .LND2 import L1D2
from .base import element, PLANE, OFF, ON
from fetk.util.geometry import edge_normal
import fetk.util.tensor as tensor


class C2D4E(element):
    """4-node isoparametric plane strain element

    Notes
    -----
    - Node and element face numbering

                   [2]
                3-------2
                |       |
           [3]  |       | [1]
                |       |
                0-------1
                   [0]

    - Element uses selectively reduced integration

    """

    signature = (PLANE, 2, ON, ON, OFF, OFF, OFF, OFF, OFF, OFF, 3, 4)
    properties = {"thickness": {"aliases": ["t", "h"], "default": 1.0}}

    def init(self):
        self.edge = L1D2(A=1)

    def area(self, xc):
        """Returns the area of the quadrilateral

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates

        Returns
        -------
        a : float
            The area

        """
        x1, x2, x3, x4 = xc[:, 0]
        y1, y2, y3, y4 = xc[:, 1]
        a = (x1 * y2 - x2 * y1) + (x2 * y3 - x3 * y2)
        a += (x3 * y4 - x4 * y3) + (x4 * y1 - x1 * y4)
        return a / 2.0

    def volume(self, xc):
        """Volume of the quadrilateral, `v = a(xc) * thickness"""
        return self.thickness * self.area(xc)

    def jacobian(self, xc, xg):
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        dNds = self.shapeder(xg)
        dxds = np.dot(dNds, xc)
        return np.linalg.det(dxds)

    def shape(self, xg):
        """Shape functions in the natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        N = np.array(
            [
                (1.0 - s) * (1.0 - t),
                (1.0 + s) * (1.0 - t),
                (1.0 + s) * (1.0 + t),
                (1.0 - s) * (1.0 + t),
            ]
        )
        return N / 4.0

    def shapeder(self, xg):
        """Derivatives of shape functions, wrt to natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        dN = np.array(
            [
                [-1.0 + t, 1.0 - t, 1.0 + t, -1.0 - t],
                [-1.0 + s, -1.0 - s, 1.0 + s, 1.0 - s],
            ]
        )
        return dN / 4.0

    def shapegrad(self, xc, xg):
        """Compute the derivatives of shape functions wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        dN : ndarray
            Derivatives of the shape function with respect to the physical
            coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        dNds = self.shapeder(xg)
        dxds = np.dot(dNds, xc)
        dsdx = np.linalg.inv(dxds)
        dNdx = np.dot(dsdx, dNds)
        return dNdx

    @property
    def gauss_points(self):
        """Gauss (integration) points

        Returns
        -------
        gp : ndarray
            s, t = gp[i] are s and t coordinates of the ith Gauss point, assuming
            counter clockwise orientation starting from the lower left corner

        """
        p = 1 / np.sqrt(3.0)
        return np.array([[-p, -p], [p, -p], [p, p], [-p, p]])

    @property
    def gauss_weights(self):
        """Gauss (integration) weights

        Returns
        -------
        gw : ndarray
            w = gw[i] is the ith Gauss weight, assuming counter clockwise
            orientation starting from the lower left corner

        """
        return np.ones(4)

    @property
    def edges(self):
        return [[0, 1], [1, 2], [2, 3], [3, 0]]

    def bmatrix(self, xc, xg):
        """Compute the element B matrix which contains derivatives of shape
        functions, wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        dNdx = self.shapegrad(xc, xg)
        num_node = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        B = np.zeros((4, num_dof_per_node * num_node))
        B[0, 0::num_dof_per_node] = B[3, 1::num_dof_per_node] = dNdx[0, :]
        B[1, 1::num_dof_per_node] = B[3, 0::num_dof_per_node] = dNdx[1, :]
        return B

    def stiffness(self, xc, material, *args):
        """Assemble the element stiffness

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        ke : ndarray
            The element stiffess
            ke = integrate(B.T, C, B)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        ke = np.zeros((num_nodes * num_dof_per_node, num_nodes * num_dof_per_node))

        Ce = material.stiffness(xc, ndir=3, nshr=1)

        # Integrate isotropic part at single integration point
        Be = self.bmatrix(xc, [0, 0])
        Je = self.jacobian(xc, [0, 0])
        Ce_iso = tensor.isotropic_part(3, 1, Ce)
        ke += np.dot(np.dot(Be.T, Ce_iso), Be) * Je * 4

        # Fully integrate deviatoric part
        p1 = p2 = 2
        Ce_dev = tensor.deviatoric_part(3, 1, Ce)
        for p in range(p1 * p2):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Be = self.bmatrix(xc, xg)
            Je = self.jacobian(xc, xg)
            Ce = material.stiffness(xc, ndir=3, nshr=1)
            ke += np.dot(np.dot(Be.T, Ce_dev), Be) * Je * wg
        return ke

    def pmatrix(self, xg):
        """Compute the element P matrix

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        P : ndarray
            The S matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        N = self.shape(xg)
        P = np.zeros((num_dof_per_node, num_nodes * num_dof_per_node))
        P[0, 0::num_dof_per_node] = N
        P[1, 1::num_dof_per_node] = N
        return P

    def force(self, xc, dload):
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        dltag : int
            Distributed load tag.  1 if a distributed load is applied
        dlvals : array_like
            The array of distributed loads
            {fx, fy} = dlvals are the components of the distributed load acting
            on the element

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        if dload is None:
            return fe
        for (tag, edge_no, f) in dload.items():
            if tag == fetk.dload.body_force:
                fe += self.body_force(xc, f[:num_dof_per_node])
            elif tag == fetk.dload.surface_force:
                fe += self.surface_force(xc, edge_no, f[:num_dof_per_node])
            elif tag == fetk.dload.pressure:
                edge = self.edges[edge_no]
                n = edge_normal(xc[edge])
                fe += self.surface_force(xc, edge_no, f[0] * n)
        return fe

    def body_force(self, xc, f):
        """Calculate the nodal force contributions from a surface load `sload`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates
        f : ndarray
            f[0] is the load on the body in the x direction
            f[1] is the load on the body in the y direction

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Je = self.jacobian(xc, xg)
            Pe = self.pmatrix(xg)
            # q = np.dot(Pe, f[:num_dof])
            fe += Je * wg * np.dot(Pe.T, f[:num_dof_per_node])
        return fe

    def surface_force(self, xc, edge_no, q):
        """Calculate the nodal force contributions from a surface load `q`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates
        edge : ndarray
            Internal node IDs of the surface
        q : ndarray
            The surface load

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        fe = np.zeros(num_nodes * num_dof_per_node)
        edge = self.edges[edge_no]
        xd = xc[edge]
        for p in range(len(self.edge.gauss_points)):
            wg = self.edge.gauss_weights[p]
            xg = self.edge.gauss_points[p]
            N = self.edge.shape(xg)
            dNds = self.edge.shapeder(xg)
            dxds = np.dot(dNds, xd)
            Jd = np.sqrt(dxds[0] ** 2 + dxds[1] ** 2)
            for (i, ni) in enumerate(edge):
                for j in range(num_dof_per_node):
                    I = ni * num_dof_per_node + j
                    fe[I] += wg * Jd * N[i] * q[j]
        return fe
