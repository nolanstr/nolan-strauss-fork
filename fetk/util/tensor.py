import numpy as np


def isotropic_part(ndir, nshr, D):
    ntens = ndir + nshr
    D1 = np.zeros((ntens, ntens))
    D1[:ndir, :ndir] = D[0, 1]
    return D1


def deviatoric_part(ndir, nshr, D):
    return D - isotropic_part(ndir, nshr, D)
