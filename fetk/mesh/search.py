import numpy as np

ILO, IHI, JLO, JHI, KLO, KHI = "ilo", "ihi", "jlo", "jhi", "klo", "khi"


def find_nodes_in_region(xc, region, map=lambda x: x + 1, tol=1e-6):
    """Find nodes in a region

    Parameters
    ----------
    xc : ndarray
        Nodal coordinates
    region : str
        Region identifier
    map : fun
        Mapping from internal node number (row number in xc) to external node ID

    Returns
    -------
    nodes : ndarray
        The node IDs of nodes in region

    """
    # Find nodes in region
    if region not in (ILO, IHI, JLO, JHI, KLO, KHI):
        raise ValueError(f"Invalid region {region!r}")
    axis, fun = {
        ILO: (0, np.amin),
        IHI: (0, np.amax),
        JLO: (1, np.amin),
        JHI: (1, np.amax),
        KLO: (2, np.amin),
        KHI: (2, np.amax),
    }[region]
    xpos = fun(xc[:, axis])
    ix = np.where(np.abs(xc[:, axis] - xpos) < tol)[0]
    return np.array([map(i) for i in ix])


def find_sides_in_region(xc, region, blocks):
    if num_dim == 3:
        raise ValueError("find_sides_in_region not implemented in 3D")
    elif num_dim == 1:
        if region not in (ILO, IHI):
            raise ValueError(f"Invalid 1D region {region}")
        inode = np.argmin(xc) if region == ILO else np.argmax(xc)
        for block in blocks:
            for (iel, el) in enumerate(block.elements):
                c = block.connect[iel]
                for (i, n) in enumerate(c):
                    if i == n:
                        return [el, i + 1]
    else:
        if region not in (ILO, IHI, JLO, JHI):
            raise ValueError(f"Invalid 2D region {region}")
        nodes = find_nodes_in_region(xc, region)
        sides = []
        for block in blocks:
            for (iel, el) in enumerate(block.elements):
                c = block.connect[iel]
                w = np.where(np.in1d(c, nodes))[0]
                if len(w) < 2:
                    continue
                w = tuple(sorted(w))
                for (d, edge) in enumerate(block.element.edges):
                    if tuple(sorted(edge)) == w:
                        # the internal node numbers match, this is the edge
                        sides.append((el, d + 1))

        return np.array(sides)
