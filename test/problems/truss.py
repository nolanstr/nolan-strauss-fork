import os
import numpy as np
import fetk.problems.truss as truss
from fetk.util.filesystem import working_dir


def test_truss_example(tmpdir, datadir):
    with working_dir(tmpdir):
        model = truss.run_file(os.path.join(datadir, "truss1.yaml"))
        expected_u = np.array(
            [
                [0.0, 0.0],
                [0.80954, -1.7756],
                [0.28, -1.79226],
                [0.899, -2.29193],
                [0.56, -2.3166],
                [0.8475, -2.38594],
                [0.8475, -2.42194],
                [0.796, -2.29193],
                [1.135, -2.3166],
                [0.88546, -1.7756],
                [1.415, -1.79226],
                [1.6950, 0.0],
            ]
        )
        expected_r = np.array(
            [
                [0.0, 28.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 28.0],
            ]
        )
        assert np.allclose(model.solution.u, expected_u)
        assert np.allclose(model.solution.r, expected_r)
