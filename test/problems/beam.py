import fetk.problems.beam as beam
from fetk.util.filesystem import working_dir


def test_beam_1(tmpdir):
    s_inp = """\
input:
  node:
  - 1, 0., 0.
  - 2, 1., 0.
  element:
  - 1, 1, 2
  element blocks:
  - block:
      name: all
      material: 1
      elements: 1
      element type: B1D2
      element properties:
        moment of inertia: 10
  materials:
  - material:
      id: 1
      model: elastic
      parameters:
        young's modulus: 10
  boundary:
  - 1, y, 0.
  - 2, y, 0.
  dload:
  - 1, py, 10.
"""
    with working_dir(tmpdir):
        beam.run_file(s_inp)
