import os
import pytest


@pytest.fixture(scope="function")
def datadir():
    root_test_dir = os.path.dirname(os.path.realpath(__file__))
    dirname = os.path.join(root_test_dir, "data")
    yield dirname
