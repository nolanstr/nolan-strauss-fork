=======================
Internal representation
=======================

Internally, distributed loads are represented by ``dltags`` and ``dlvals`` describing the loads at each node.  ``dltags`` and ``dlvals`` should be constructed by ``fetk.dload.fe_objects``.

-----------------
dltags and dlvals
-----------------

``dlftags`` is a ``num_elem`` by ``max_dload`` table containing an integer flag defining the type of load condition that is applied to each degree of freedom at each node.  The values are 0 for no load and 1 for a load.  ``dlvals`` is a ``num_elem`` by ``max_dload`` table containing the corresponding magnitudes of the loads prescribed in ``dltags``.

.. rubric:: Example

A bar oriented along the x direction having 5 elements has a constant distributed load :math:`q=-1000` applied along its length.

.. code-block:: python

   import numpy as np

   q = -1000
   dltags = np.array(
       [
           [1, 1, 0, 0, 0],
           [2, 1, 0, 0, 0],
           [3, 1, 0, 0, 0],
           [4, 1, 0, 0, 0],
           [5, 1, 0, 0, 0],
       ], dtype=int
   )
   dlvals = np.array(
       [
           [1, q, 0, 0, 0],
           [2, q, 0, 0, 0],
           [3, q, 0, 0, 0],
           [4, q, 0, 0, 0],
           [5, q, 0, 0, 0],
       ], dtype=int
   )

----------
Generating
----------

``dltags`` and ``dlvals`` are easily generated from the element and dload tables:

.. code-block:: python

    import numpy as np
    from fetk.constants import max_dload

    dlvals = np.zeros((len(nodes), max_dload))
    dltags = np.zeros((len(nodes), max_dload), dtype=int)
    for condition in dload:
        xe, dof, magnitude = condition
        e = elem_map[xe]
        dltags[e, dof] = type
        dlvals[e, dof] = magnitude

``fetk.dload.fe_objects`` implements the above algorithm with additional checks for correctness of input and can be used to generate ``dltags`` and ``dltags`` from user defined boundary data.
