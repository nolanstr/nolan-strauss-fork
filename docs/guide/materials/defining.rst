==================
Defining materials
==================

Materials are defined by a list of materials used in the finite element model

.. code-block:: python

   materials = [material_0, material_1, . . ., material_n]

Each component of ``materials`` is a mapping defining the material model and
contains the model name, material ID, and constitutive properties required by
the material.

.. rubric:: Example

The following defines a one-dimensional elastic material model

.. code-block:: python

   materials = [
       {
           "id": 1,
           "model": "elastic",
           "parameters": {"modulus": 10.0e9}
       }
   ]

See :ref:`mat_models` for this available material models.
