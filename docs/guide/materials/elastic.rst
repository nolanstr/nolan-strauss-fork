=================
Linear elasticity
=================

The ``elastic`` material model defines a basic linear elastic response.

.. rubric:: Model name: elastic

.. rubric:: Parameters:

* modulus: Young's modulus

----------------
Input file usage
----------------

.. code-block:: yaml

    materials:
    - material:
      id: INT
      model: elastic
      parameters:
        modulus: REAL
