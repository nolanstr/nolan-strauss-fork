.. _eb_description:

==============
Element blocks
==============

Elements in the finite element model are grouped into element blocks.  Every
element must belong to one, and only one, element block. Within an element
block, all elements are of the same type (basic geometry and number of nodes)
and have the same material.  This definition does not preclude multiple element
blocks containing the same element type (i.e., “QUAD” elements may be in more
than one element block); only that each element block may contain only one
element type.

Elements are numbered internally (beginning with 0) consecutively across all
element blocks.
