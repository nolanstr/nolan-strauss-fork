=====================
Using fem: input file
=====================

The input file is the means of communication between the user and ``fem``.  It
contains a complete description of the numerical model. The input file uses yaml
as the markup language and is easy to write and modify using a text editor.

.. rubric:: Contents

.. toctree::
   :maxdepth: 1

   model
   format
